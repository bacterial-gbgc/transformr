PKG = transformr_0.0.2.tar.gz

.PHONY: check install build install-build vignettes

bioc-check: build
	R CMD BiocCheck $(PKG)

check: build
	R CMD check $(PKG)

install-build: build
	R CMD INSTALL --build $(PKG)


.PHONY: vignettes
vignettes:
	$(MAKE) -C vignettes

build:
	@rm -f $(PKG)
	R CMD build .
