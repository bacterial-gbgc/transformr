[![License: GPL v3](https://img.shields.io/badge/License-GPL3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

# Transformr

Analysis of Whole Genome Transformation assays within R.
